package com.gitee.sunchenbin.model;


import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 全部采用actable自有的注解
 */
@Table
public class ACTableSimple {


    private Long id;

    @Column
    private String name;

    @Column
    private Date createTime;

    @Column
    private Boolean isTrue;

    @Column
    private Integer age;

    @Column
    private BigDecimal price;

    @Column
    private String identitycard;

}
