package com.gitee.sunchenbin.model;

import com.lesterlaucn.mybatis.migration.constants.MySQLCharsetEnum;
import com.lesterlaucn.mybatis.migration.constants.MySQLEngineEnum;
import com.lesterlaucn.mybatis.migration.constants.MySQLTypeEnum;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 全部采用actable自有的注解
 */
@Table
public class ACTableAdvance {


    private Long id;

    private String name;

    private Date createTime;

    @Column
    private Boolean isTrue;

    @Column

    private Integer age;

    @Column
    private BigDecimal price;

    @Column
    private String identitycard;

    @Column
    private String shop;

}
