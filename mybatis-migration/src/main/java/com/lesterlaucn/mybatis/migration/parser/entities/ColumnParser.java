package com.lesterlaucn.mybatis.migration.parser.entities;


import com.lesterlaucn.mybatis.migration.command.JavaToMysqlType;
import com.lesterlaucn.mybatis.migration.command.MySqlTypeAndLength;
import com.lesterlaucn.mybatis.migration.constants.MySQLCharsetEnum;
import com.lesterlaucn.mybatis.migration.constants.MySQLEngineEnum;
import com.lesterlaucn.mybatis.migration.constants.MySQLTypeEnum;
import com.google.common.base.CaseFormat;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ColumnParser {

    public static final String DEFAULTVALUE = "DEFAULT";

    public static final String SQL_ESCAPE_CHARACTER = "`";

    public static String getTableName(Class<?> clasz) {
        Table tableName = clasz.getAnnotation(Table.class);
        Table tableNameCommon = clasz.getAnnotation(Table.class);
        if (!hasTableAnnotation(clasz)) {
            return null;
        }
        String finalTableName = "";
        if (tableName != null && !StringUtils.isEmpty(tableName.name())) {
            finalTableName = tableName.name();
        }
        if (tableNameCommon != null && !StringUtils.isEmpty(tableNameCommon.name())) {
            finalTableName = tableNameCommon.name();
        }
        if (StringUtils.isEmpty(finalTableName)) {
            // 都为空时采用类名按照驼峰格式转会为表名
            finalTableName = getBuildLowerName(clasz.getSimpleName());
        }
        return finalTableName;
    }

    public static String getTableComment(Class<?> clasz) {
        Table table = clasz.getAnnotation(Table.class);
        if (!hasTableAnnotation(clasz)) {
            return "";
        }
        return "";
    }

    public static MySQLCharsetEnum getTableCharset(Class<?> clasz) {
        Table table = clasz.getAnnotation(Table.class);
        if (!hasTableAnnotation(clasz)) {
            return null;
        }
        return null;
    }

    public static MySQLEngineEnum getTableEngine(Class<?> clasz) {
        Table table = clasz.getAnnotation(Table.class);
        if (!hasTableAnnotation(clasz)) {
            return null;
        }
        if (table != null && table.engine() != MySQLEngineEnum.DEFAULT) {
            return table.engine();
        }
        if (engine != null && !StringUtils.isEmpty(engine.value())) {
            return engine.value();
        }
        return null;
    }

    public static String getColumnName(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        Column columnCommon = field.getAnnotation(javax.persistence.Column.class);

        if (!hasColumnAnnotation(field, clasz)) {
            return null;
        }
        if (column != null && !StringUtils.isEmpty(column.name())) {
            return column.name().toLowerCase().replace(SQL_ESCAPE_CHARACTER, "");
        }

        if (columnCommon != null && !StringUtils.isEmpty(columnCommon.name())) {
            return columnCommon.name().toLowerCase().replace(SQL_ESCAPE_CHARACTER, "");
        }

        return getBuildLowerName(field.getName()).replace(SQL_ESCAPE_CHARACTER, "");
    }

    private static String getBuildLowerName(String name) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,
                name).toLowerCase();
    }

    public static boolean isKey(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        if (!hasColumnAnnotation(field, clasz)) {
            return false;
        }
        IsKey isKey = field.getAnnotation(IsKey.class);
        Id id = field.getAnnotation(Id.class);
        if (null != isKey) {
            return true;
        } else if (column != null && column.isKey()) {
            return true;
        } else if (null != id) {
            return true;
        } else if (null != tableId) {
            return true;
        }
        return false;
    }

    public static boolean isAutoIncrement(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        if (!hasColumnAnnotation(field, clasz)) {
            return false;
        }
        IsAutoIncrement isAutoIncrement = field.getAnnotation(IsAutoIncrement.class);
        if (null != isAutoIncrement) {
            return true;
        } else if (column != null && column.isAutoIncrement()) {
            return true;
        }
        return false;
    }

    public static Boolean isNull(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        javax.persistence.Column columnCommon = field.getAnnotation(javax.persistence.Column.class);
        if (!hasColumnAnnotation(field, clasz)) {
            return true;
        }
        boolean iskey = isKey(field, clasz);
        // 主键默认为非空
        if (iskey) {
            return false;
        }

        IsNotNull isNotNull = field.getAnnotation(IsNotNull.class);
        if (null != isNotNull) {
            return false;
        } else if (column != null) {
            return column.isNull();
        } else if (columnCommon != null) {
            return columnCommon.nullable();
        }
        return true;
    }

    public static String getComment(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        ColumnComment comment = field.getAnnotation(ColumnComment.class);
        if (!hasColumnAnnotation(field, clasz)) {
            return null;
        }
        if (column != null && !StringUtils.isEmpty(column.comment())) {
            return column.comment();
        }
        if (comment != null && !StringUtils.isEmpty(comment.value())) {
            return comment.value();
        }
        return "";
    }

    public static String getDefaultValue(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        DefaultValue defaultValue = field.getAnnotation(DefaultValue.class);
        if (!hasColumnAnnotation(field, clasz)) {
            return null;
        }
        if (column != null && !DEFAULTVALUE.equals(column.defaultValue())) {
            return column.defaultValue();
        }
        if (defaultValue != null) {
            return defaultValue.value();
        }
        return null;
    }

    public static boolean getDefaultValueNative(Field field, Class<?> clasz) {
        IsNativeDefValue isNativeDefValue = field.getAnnotation(IsNativeDefValue.class);
        if (isNativeDefValue != null) {
            return isNativeDefValue.value();
        }
        if (field.getGenericType().toString().equals("class java.lang.String")
                || field.getGenericType().toString().equals("char")
                || field.getGenericType().toString().equals("class java.lang.Boolean")
                || field.getGenericType().toString().equals("boolean")) {
            return false;
        }
        return true;
    }

    public static MySqlTypeAndLength getMySqlTypeAndLength(Field field, Class<?> clasz) {
        Column column = getColumn(field, clasz);
        javax.persistence.Column columnCommon = field.getAnnotation(javax.persistence.Column.class);
        ColumnType type = field.getAnnotation(ColumnType.class);
        if (!hasColumnAnnotation(field, clasz)) {
            throw new RuntimeException("字段名：" + field.getName() + "没有字段标识的注解，异常抛出！");
        }
        if (column != null && column.type() != MySQLTypeEnum.DEFAULT) {
            return buildMySqlTypeAndLength(field, column.type().toString().toLowerCase(), column.length(), column.decimalLength());
        }
        if (type != null && type.value() != null && type.value() != MySQLTypeEnum.DEFAULT) {
            return buildMySqlTypeAndLength(field, type.value().toString().toLowerCase(), type.length(), type.decimalLength());
        }
        if (type != null && columnCommon != null && type.value() != null && type.value() != MySQLTypeEnum.DEFAULT) {
            return buildMySqlTypeAndLength(field, type.value().toString().toLowerCase(), columnCommon.length(), columnCommon.scale());
        }
        // 类型为空根据字段类型去默认匹配类型
        MySQLTypeEnum mysqlType = JavaToMysqlType.javaToMysqlTypeMap.get(field.getGenericType().toString());
        if (mysqlType == null) {
            throw new RuntimeException("字段名：" + field.getName() + "不支持" + field.getGenericType().toString() + "类型转换到mysql类型，仅支持JavaToMysqlType类中的类型默认转换，异常抛出！");
        }
        String sqlType = mysqlType.toString().toLowerCase();
        // 默认类型可以使用column来设置长度
        if (column != null) {
            return buildMySqlTypeAndLength(field, sqlType, column.length(), column.decimalLength());
        }
        // 默认类型可以使用type来设置长度
        if (type != null) {
            return buildMySqlTypeAndLength(field, sqlType, type.length(), type.decimalLength());
        }
        // 默认类型可以使用columnCommon来设置长度
        if (columnCommon != null) {
            return buildMySqlTypeAndLength(field, sqlType, columnCommon.length(), columnCommon.scale());
        }
        return buildMySqlTypeAndLength(field, sqlType, 255, 0);
    }

    private static MySqlTypeAndLength buildMySqlTypeAndLength(Field field, String type, int length, int decimalLength) {
        MySqlTypeAndLength mySqlTypeAndLength = MySQLTypeEnum.mySqlTypeAndLengthMap.get(type);
        if (mySqlTypeAndLength == null) {
            throw new RuntimeException("字段名：" + field.getName() + "使用的" + type + "类型，没有配置对应的MySqlTypeConstant，只支持创建MySqlTypeConstant中类型的字段，异常抛出！");
        }
        MySqlTypeAndLength targetMySqlTypeAndLength = new MySqlTypeAndLength();
        BeanUtils.copyProperties(mySqlTypeAndLength, targetMySqlTypeAndLength);
        if (length != 255) {
            targetMySqlTypeAndLength.setLength(length);
        }
        if (decimalLength != 0) {
            targetMySqlTypeAndLength.setDecimalLength(decimalLength);
        }
        return targetMySqlTypeAndLength;
    }

    public static boolean hasTableAnnotation(Class<?> clasz) {
        Table tableName = clasz.getAnnotation(Table.class);
        javax.persistence.Table tableNameCommon = clasz.getAnnotation(javax.persistence.Table.class);
        if (tableName == null && tableNameCommon == null && tableNamePlus == null) {
            return false;
        }
        return true;
    }

    public static boolean hasIgnoreTableAnnotation(Class<?> clasz) {
        IgnoreTable ignoreTable = clasz.getAnnotation(IgnoreTable.class);
        if (ignoreTable == null) {
            return false;
        }
        return true;
    }

    public static boolean hasColumnAnnotation(Field field, Class<?> clasz) {
        // 是否开启simple模式
        boolean isSimple = isSimple(clasz);
        // 不参与建表的字段
        String[] excludeFields = excludeFields(clasz);
        // 当前属性名在排除建表的字段内
        if (Arrays.asList(excludeFields).contains(field.getName())) {
            return false;
        }
        Column column = field.getAnnotation(Column.class);
        Column columnCommon = field.getAnnotation(Column.class);
        TableField tableField = field.getAnnotation(TableField.class);
        IsKey isKey = field.getAnnotation(IsKey.class);
        Id id = field.getAnnotation(Id.class);
        TableId tableId = field.getAnnotation(TableId.class);
        if (column == null && columnCommon == null && (tableField == null || !tableField.exist())
                && isKey == null && id == null && tableId == null) {
            // 开启了simple模式
            if (isSimple) {
                return true;
            }
            return false;
        }
        return true;
    }

    private static Column getColumn(Field field, Class<?> clasz) {
        // 不参与建表的字段
        String[] excludeFields = excludeFields(clasz);
        if (Arrays.asList(excludeFields).contains(field.getName())) {
            return null;
        }
        Column column = field.getAnnotation(Column.class);
        if (column != null) {
            return column;
        }
        // 是否开启simple模式
        boolean isSimple = isSimple(clasz);
        // 开启了simple模式
        if (isSimple) {
            return new ColumnImpl();
        }
        return null;
    }

    private static String[] excludeFields(Class<?> clasz) {
        String[] excludeFields = {};
        Table tableName = clasz.getAnnotation(Table.class);
        if (tableName != null) {
            excludeFields = tableName.excludeFields();
        }
        return excludeFields;
    }

    private static boolean isSimple(Class<?> clasz) {
        boolean isSimple = false;
        Table tableName = clasz.getAnnotation(Table.class);
        if (tableName != null) {
            isSimple = tableName.isSimple();
        }
        return isSimple;
    }


}
